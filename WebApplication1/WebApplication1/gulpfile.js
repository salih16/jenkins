﻿/// <reference path="C:\Users\salihbicakci.EVIDEA\Documents\Source\Repos\jenkins\WebApplication1\WebApplication1\Scripts/jquery-1.10.2.js" />
/// <reference path="C:\Users\salihbicakci.EVIDEA\Documents\Source\Repos\jenkins\WebApplication1\WebApplication1\Scripts/jquery-1.10.2.js" />
var gulp   = require("gulp");
var concat = require("gulp-concat");
var cssmin = require("gulp-cssmin");
var uglify = require("gulp-uglify");
var rename = require("gulp-rename");
var less   = require("gulp-less");

//gulp.task("Minify:Desktop:Css", function () {
//    console.log("Desktop CSS Minify Started");
//    return gulp
//        .src("./Content/evidea-styles.css")
//        .pipe(cssmin({
//            compatibility: "*",
//            level: {
//                1: {
//                    cleanupCharsets: true, // controls `@charset` moving to the front of a stylesheet; defaults to `true`
//                    normalizeUrls: true, // controls URL normalization; defaults to `true`
//                    optimizeBackground: false, // controls `background` property optimizatons; defaults to `true`
//                    optimizeBorderRadius: true, // controls `border-radius` property optimizatons; defaults to `true`
//                    optimizeFilter: false, // controls `filter` property optimizatons; defaults to `true`
//                    optimizeFont: true, // controls `font` property optimizatons; defaults to `true`
//                    optimizeFontWeight: true, // controls `font-weight` property optimizatons; defaults to `true`
//                    optimizeOutline: true, // controls `outline` property optimizatons; defaults to `true`
//                    removeEmpty: true, // controls removing empty rules and nested blocks; defaults to `true`
//                    removeNegativePaddings: true, // controls removing negative paddings; defaults to `true`
//                    removeQuotes: true, // controls removing quotes when unnecessary; defaults to `true`
//                    removeWhitespace: true, // controls removing unused whitespace; defaults to `true`
//                    replaceMultipleZeros: true, // contols removing redundant zeros; defaults to `true`
//                    replaceTimeUnits: true, // controls replacing time units with shorter values; defaults to `true`
//                    replaceZeroUnits: true, // controls replacing zero values with units; defaults to `true`
//                    roundingPrecision: false, // rounds pixel values to `N` decimal places; `false` disables rounding; defaults to `false`
//                    selectorsSortingMethod: 'standard', // denotes selector sorting method; can be `natural` or `standard`; defaults to `standard`
//                    specialComments: 'all', // denotes a number of /*! ... */ comments preserved; defaults to `all`
//                    tidyAtRules: true, // controls at-rules (e.g. `@charset`, `@import`) optimizing; defaults to `true`
//                    tidyBlockScopes: true, // controls block scopes (e.g. `@media`) optimizing; defaults to `true`
//                    tidySelectors: true, // controls selectors optimizing; defaults to `true`,
//                    transform: function () { } // defines a callback for fine-grained property optimization; defaults to no-op
//                }
//            }
//        }))
//        .pipe(rename({ suffix: ".min" }))
//        .pipe(gulp.dest("./Content/"));
//});

gulp.task("Minify:Desktop:Css", function () {
    console.log("Desktop Css Minify Started");
    return gulp
        .src('**/*.css', { base : './Content/' })
        .pipe(concat({ path: "minify.css" }))
        .pipe(gulp.dest("./Content/"))
        .pipe(cssmin({ level: 1}))
        .pipe(rename({ suffix: ".min"}))
        .pipe(gulp.dest("./Content/"));
});

gulp.task("Minify:web:Js:Part1", function () {
    return gulp
        .src(["./Scripts/bootstrap.js", "./Scripts/jquery-1.10.2.js"])
        .pipe(concat({ path: "minify.js" }))
        .pipe(gulp.dest("./Scripts/"))
        .pipe(uglify())
        .pipe(rename({ suffix: ".min" }))
        .pipe(gulp.dest("./Scripts/"));
});

//gulp.task("Minify:Mobile:Css", function () {
//    console.log("Mobile CSS Minify Started");
//    return gulp
//        .src("./Assets/css/styles.less")
//        .pipe(less({
//           compress: true
//        }))
//        .pipe(rename({ suffix: ".min" }))
//        .pipe(gulp.dest("./Assets/css/"));
//});

//gulp.task("Minify:Mobile:Js:Part1", function () {
//    console.log("Mobile JS Minify Part 1 Started");
//    return gulp
//        .src([
//            "./Assets/js/jquery-1.12.4.min.js",
//            "./Assets/js/jquery-migrate-1.2.1.js",
//            "./Assets/js/jquery-ui.min.js",
//            "./Assets/js/modernizr.js",
//            "./Assets/js/jquery.bxslider.js",
//            "./Assets/js/underscore-min.js",
//            "./Assets/js/jstorage.min.js",
//            "./Assets/js/jquery.loadTemplate-1.5.7.min.js",
//            "./Assets/js/jquery.validate.min.js",
//            "./Assets/js/additional-methods.min.js",
//            "./Assets/js/jquery.browser.js",
//            "./Assets/js/jquery.lazyload.min.js"
//        ])
//        .pipe(concat({ path: "evidea-frameworks-bundle.js" }))
//        .pipe(gulp.dest("./Assets/js/"))
//        .pipe(uglify())
//        .pipe(rename({ suffix: ".min" }))
//        .pipe(gulp.dest("./Assets/js/"));
//});

//gulp.task("Minify:Mobile:Js:Part2", function () {
//    console.log("Mobile JS Minify Part 2 Started");
//    return gulp
//        .src([
//            "./Assets/js/functions.js",
//            "./Assets/js/common.js"
//        ])
//        .pipe(concat({ path: "evidea-custom-bundle.js" }))
//        .pipe(gulp.dest("./Assets/js/"))
//        .pipe(uglify())
//        .pipe(rename({ suffix: ".min" }))
//        .pipe(gulp.dest("./Assets/js/"));
//});

gulp.task("default", ["Minify:Desktop:Css","Minify:web:Js:Part1"], function () {
    /* Yukarıdaki tüm minify işlemlerini tek 1 adımda başlatır. */
});